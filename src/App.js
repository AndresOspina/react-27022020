import React, { useContext } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { AuthContext } from './context/AuthContext';

import Home from './components/Home';
import NotFound from './components/NotFound';
import AuthForm from './components/AuthForm';
import Protected from './components/Protected';
import SecureRoute from './components/SecureRoute';

import './App.scss';

function App() {
  const isAuthenticated = useContext(AuthContext);

  if (isAuthenticated === null) {
    return <h3>Loading...</h3>;
  }

  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <SecureRoute exact path="/protected" component={Protected} />
          <Route exact path="/signin" component={AuthForm} />
          <Route exact path="/" component={Home} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
