import React from 'react';
import ReactDOM from 'react-dom';
import { CookiesProvider } from 'react-cookie';

import { AuthContextProvider } from './context/AuthContext';
import App from './App';

import './index.scss';

ReactDOM.render(
  <CookiesProvider>
    <AuthContextProvider>
      <App />
    </AuthContextProvider>
  </CookiesProvider>,
  document.getElementById('root')
);
