import React, { useState, useEffect } from 'react';
import { useCookies } from 'react-cookie';

import { register, login } from '../api/auth';

function AuthForm({ history }) {
  const [cookies, setCookie] = useCookies(['token']);
  const { token } = cookies;

  const [isLogin, setIsLogin] = useState(false);
  const [error, setError] = useState(null);
  const [formState, setFormState] = useState({
    email: '',
    password: ''
  });

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      const authFn = isLogin ? login : register;
      const res = await authFn(formState);
      setCookie('token', res.token);
    } catch (e) {
      setError(e.message);
    }
  }

  useEffect(() => {
    if (token) {
      history.push('/protected');
    }
  }, [token, history]);

  return (
    <form onSubmit={handleSubmit}>
      <h2>{isLogin ? 'Login' : 'Register'} in our super cool App</h2>
      <button type="button" onClick={() => setIsLogin(!isLogin)}>
        Change to {isLogin ? 'Register' : 'Login'}
      </button>
      <label htmlFor="email">
        <p>Introduce your email</p>
        <input
          type="email"
          name="email"
          value={formState.email}
          onChange={e => {
            const { value } = e.target;
            setFormState({ ...formState, email: value });
          }}
        />
      </label>
      <label htmlFor="password">
        <p>Introduce your password</p>
        <input
          type="password"
          name="password"
          value={formState.password}
          onChange={e => {
            const { value } = e.target;
            setFormState({ ...formState, password: value });
          }}
        />
      </label>
      <button
        disabled={!formState.email || !formState.password}
        style={{ display: 'block', margin: '1rem auto' }}
        type="submit"
      >
        Submit form
      </button>
      {error ? (
        <p style={{ color: 'red', fontWeight: 'bold' }}>{error}</p>
      ) : null}
    </form>
  );
}

export default AuthForm;
