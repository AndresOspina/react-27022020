import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { AuthContext } from '../context/AuthContext';

function SecureRoute(props) {
  const isAuthenticated = useContext(AuthContext);

  return isAuthenticated ? <Route {...props} /> : <Redirect to="/signin" />;
}

export default SecureRoute;
