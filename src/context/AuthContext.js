import React, { useState, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import { isLoggedIn } from '../api/auth';

export const AuthContext = React.createContext();

export function AuthContextProvider({ children }) {
  const [isAuthenticated, setIsAuthenticated] = useState(null);

  const [cookies] = useCookies(['token']);
  const { token } = cookies;

  useEffect(() => {
    async function checkAuth() {
      try {
        await isLoggedIn(token);
        setIsAuthenticated(true);
      } catch (e) {
        setIsAuthenticated(false);
      }
    }

    checkAuth();
  }, [token]);

  return (
    <AuthContext.Provider value={isAuthenticated}>
      {children}
    </AuthContext.Provider>
  );
}
